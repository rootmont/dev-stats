import json
import requests
import logging
import re
import time
import datetime
from typing import Union, List, Callable

from requests.auth import HTTPBasicAuth
from common.env import *
from common.db.github_db import get_for_coin_id, insert_for_github_data, GithubCommit, GithubData, GithubIssue, GithubComment, \
    parse_github_date


logger = logging.getLogger('gathering')
base_url_stem = 'https://api.github.com/'
repo_url_stem = 'https://api.github.com/repos/'
project_url_stem = 'https://api.github.com/orgs/'


def _parse_rate_limit(res):
    if res is None or res.headers.get("X-RateLimit-Remaining") is None:
        return
    remaining = int(res.headers.get("X-RateLimit-Remaining"))
    reset = int(res.headers.get("X-RateLimit-Reset"))
    if remaining <= 2:
        logger.info("Github rate limit almost reached - waiting {} seconds until next round."
                     .format(reset - int(time.time())))
        if reset - int(time.time()) > 0:
            time.sleep((reset + 100) - int(time.time()))
            logger.info("Done waiting for Github API rate limit - resuming.")


def github_request(url: str, headers=None):
    resp = requests.get(url, auth=HTTPBasicAuth(GITHUBUSER, GITHUBPW), headers=headers)
    _parse_rate_limit(resp)
    return resp


def get_github_data(repo_url: str):
    """
    Main entry point to collect git data
    :param repo_url: url of the repo / project page to scrape
    :return: Map <size / stargazers / forks / open_issues / last_commit />
    """
    # Only get stats for the single repository in this case
    if not _is_github_org(repo_url):
        ret = {}
        data_url = repo_url_stem + get_url_suffix(repo_url)
        data = json.loads(github_request(data_url).text)
        try:
            ret['size'] = data['size']
            ret['stargazers'] = data['stargazers_count']
            ret['forks'] = int(data['forks_count'])
            ret['open_issues'] = data['open_issues']
            ret['last_commit'] = data['pushed_at'].split('T')[0]
            ret.update(get_contributor_data(data['contributors_url']))
        except (KeyError, TypeError, json.JSONDecodeError):
            return None
        return ret

    logger.info("Processing {} as an organization".format(repo_url))
    project_info = _get_org_page_information(repo_url)
    if project_info is None:
        logger.warning("Url: {} was not actually an organization.".format(repo_url))
        return None
    repos_info = github_request(project_info.get("repos_url"))

    if repos_info.status_code != 200:
        logger.warning("Couldn't get repository information for url: {}".format(project_info.get("repos_url")))
        return None

    repos_info_json = json.loads(repos_info.text)
    ret = {'size': 0, 'stargazers': 0, 'forks': 0, 'open_issues': 0, 'last_commit': None, 'contributors': 0, 'contributions': 0}
    for repo_info in repos_info_json:
        ret['size'] += repo_info['size']
        ret['stargazers'] += repo_info['stargazers_count']
        ret['forks'] += int(repo_info['forks_count'])
        ret['open_issues'] += repo_info['open_issues']
        pushed_at = repo_info['pushed_at'].split('T')[0]
        if ret.get('last_commit') is None or \
                datetime.datetime.strptime(pushed_at, "%Y-%m-%d") > datetime.datetime.strptime(ret["last_commit"], "%Y-%m-%d"):
            ret['last_commit'] = pushed_at
        contributor_data = get_contributor_data(repo_info['contributors_url'])
        ret['contributors'] += contributor_data['contributors']
        ret['contributions'] += contributor_data['contributions']
    return ret


def get_url_suffix(repo_url):
    parts = repo_url.split('/')
    if parts[-1] == '':
        return '/'.join(parts[-3:-1])
    return '/'.join(parts[-2:])


def get_contributor_data(url: str):
    """
    Non-historical data, scrolls through pages of contributors and accumulates numbers
    :param url: repo url to get
    :return:
    """
    data = {
        'contributors': 0,
        'contributions': 0
    }
    page = 0
    while True:
        res = json.loads(github_request('%s?page=%d' % (url, page)).text)
        if len(res) == 0:
            break
        page += 1
        data['contributors'] += len(res)
        data['contributions'] += sum([x['contributions'] for x in res])
    return data


def _get_org_page_information(url: str) -> Union[dict, None]:
    ident: str = _get_last_identifier(url)
    url = project_url_stem + ident
    req = github_request(url)
    if req.status_code != 200:
        return None
    return json.loads(req.text)


def _get_last_identifier(url: str) -> str:
    """
    Last identifier is only significant for github organizations
    :param url: url to scrape the last identifier for
    :return: str, last itentifier of the provided organization
    """
    split = url.split("/")
    return split[len(split) - 1]


def _is_github_org(url: str) -> bool:
    split = url.split("/")
    for index, text in enumerate(split):
        if "github.com" in text.lower() and len(split) - index - 1 == 1:
            info = _get_org_page_information(url)
            return info is not None and info.get('type') is not None and info.get('type').lower() == "organization".lower()
    return False


def _get_repo_id(repo_url: str) -> int:
    """
    Gets the repository id for the respective repo url
    :param repo_url: url for the repo to get the url for
    :return: int, the id of the provided repository
    """
    data_url = repo_url_stem + get_url_suffix(repo_url)
    data = json.loads(github_request(data_url).text)
    return int(data.get("id"))


def _parse_link_headers(resp) -> (int, int):
    # Get subsequent star pages, should be no additional links when stars < 30
    links = resp.headers.get('Link')
    if links is None or links == "":
        return 0, 0
    found_links = re.findall(r'http[\w\d:/.?=]+', links)
    if found_links is None or len(found_links) != 2:
        return 0, 0
    # Get the first and last page numbers, if we can't find them then return what we have
    next_page_num = int(re.search(r'[\d]+$', found_links[0])[0]) if re.search(r'[\d]+$', found_links[1]) else 0
    last_page_num = int(re.search(r'[\d]+$', found_links[1])[0]) if re.search(r'[\d]+$', found_links[0]) else 0
    return next_page_num, last_page_num


def _historical_parsing(repo_url: str, end_point: str, accumulator: Callable, breaker: Callable, sorter: Callable, backward=True) -> List:
    suffix = get_url_suffix(repo_url)
    headers = {"Accept": "application/vnd.github.v3.star+json"}
    full_url = repo_url_stem + suffix + end_point
    resp = github_request(full_url, headers=headers)
    if resp.status_code != 200:
        logger.warning("Could not get forks information for repository {}".format(suffix))
        return []

    # Get subsequent star pages, should be no additional links when stars < 30
    _, last_page_num = _parse_link_headers(resp)
    if last_page_num == 0:
        last_page_num = 1

    accum = []

    # We care about this because some pages list the most recent first and some most recent last...
    if backward:
        start = last_page_num + 1
        finish = 0
        incr = -1
    else:
        start = 1
        finish = last_page_num + 1
        incr = 1

    # we go backwards here so we can break on the case we are up to date
    for n in range(start, finish, incr):
        resp = github_request(full_url + "?page=" + str(n), headers=headers)
        if resp.status_code != 200:
            continue
        j = sorter(json.loads(resp.text))
        for d in accumulator(j):
            # if we encounter the same element we want to break query / execution
            if breaker(d):
                return accum
            accum.append(d)

    return accum


def _historical_issues(repo_url: str, github_data: GithubData = None) -> List[GithubIssue]:
    def sorter(total_json):
        total_json.sort(key=lambda x: parse_github_date(x["created_at"]), reverse=True)
        return total_json

    def accumulator(j):
        for issue in j:
            yield GithubIssue(issue["created_at"], issue.get("closed_at"))

    def breaker(i: GithubIssue) -> bool:
        if github_data is None:
            return False
        if i in github_data.issues:
            return True
        return False

    his = _historical_parsing(repo_url, "/issues", accumulator, breaker, sorter)

    if github_data is not None:
        return [*github_data.issues, *his]
    return his


def _historical_forks(repo_url: str, github_data: GithubData = None) -> List[datetime.datetime]:
    def sorter(total_json):
        total_json.sort(key=lambda x: parse_github_date(x["created_at"]), reverse=True)
        return total_json

    def accumulator(fork_json):
        for fork in fork_json:
            yield parse_github_date(fork["created_at"])

    def breaker(d: datetime.datetime) -> bool:
        if github_data is None:
            return False
        if d in github_data.forks:
            return True
        return False

    his = _historical_parsing(repo_url, "/forks", accumulator=accumulator, breaker=breaker, sorter=sorter, backward=False)

    if github_data is not None:
        return [*github_data.forks, *his]

    return his


def _historical_stars(repo_url: str, github_data: GithubData = None) -> List[datetime.datetime]:
    def sorter(total_json):
        total_json.sort(key=lambda x: parse_github_date(x["starred_at"]), reverse=True)
        return total_json

    def accumulator(j):
        for star in j:
            yield parse_github_date(star["starred_at"])

    def breaker(d: datetime.datetime) -> bool:
        if github_data is None:
            return False
        if d in github_data.stars:
            return True
        return False

    his = _historical_parsing(repo_url, "/stargazers", accumulator, breaker, sorter)

    if github_data is not None:
        return [*github_data.stars, *his]

    return his


def _historical_commits(repo_url: str, github_data: GithubData = None) -> List[GithubCommit]:
    def sorter(total_json):
        total_json.sort(key=lambda x: parse_github_date(x["commit"]["committer"]["date"]), reverse=True)
        return total_json

    def accumulator(j):
        for commit in j:
            d = commit["commit"]["committer"]["date"]
            if commit["author"] is not None and commit["author"].get("login") is not None:
                a = commit["author"]["login"]
            elif commit["commit"].get("author") is not None and commit["commit"].get("author").get("name") is not None:
                a = commit["commit"]["author"]["name"]
            else:
                a = ""
            yield GithubCommit(a, d)

    def breaker(c: GithubCommit) -> bool:
        if github_data is None:
            return False
        if c in github_data.commits:
            return True
        return False

    his = _historical_parsing(repo_url, "/commits", accumulator=accumulator, breaker=breaker, sorter=sorter, backward=False)

    if github_data is not None:
        return [*github_data.commits, *his]

    return his


def _historical_comments(repo_url: str, github_data: GithubData=None) -> List[GithubComment]:
    def sorter(total_json):
        total_json.sort(key=lambda x: parse_github_date(x["created_at"]), reverse=True)
        return total_json

    def accumulator(j):
        for comment in j:
            if comment.get("author") is not None:
                a = comment["author"]["login"]
            else:
                a = comment["user"]["login"]
            yield GithubComment(a, comment["created_at"])

    def breaker(c: GithubComment) -> bool:
        if github_data is None:
            return False
        if c in github_data.comments:
            return True
        return False

    his = _historical_parsing(repo_url, "/comments", accumulator=accumulator, breaker=breaker, sorter=sorter)

    if github_data is not None:
        return [*github_data.comments, *his]

    return his


def _get_all_historical_data(coin_id: int, repo_url: str, github_data=None) -> GithubData:
    # Only get stats for the single repository in this case
    logger.info("  Stars for github for coin: {}".format(repo_url))
    stars = _historical_stars(repo_url, github_data)
    logger.info("  Forks for github for coin: {}".format(repo_url))
    forks = _historical_forks(repo_url, github_data)
    logger.info("  Issues for github for coin: {}".format(repo_url))
    issues = _historical_issues(repo_url, github_data)
    logger.info("  Commits for github for coin: {}".format(repo_url))
    commits = _historical_commits(repo_url, github_data)
    logger.info("  Comments for github for coin: {}".format(repo_url))
    comments = _historical_comments(repo_url, github_data)
    return GithubData(coin_id, repo_url, forks, stars, issues, commits, comments)


def _accumulate_historical_data(coin_id: int, org_url: str, github_data=None) -> Union[GithubData, None]:
    logger.info("Processing {} as an organization".format(org_url))
    project_info = _get_org_page_information(org_url)
    if project_info is None:
        logger.warning("Url: {} was not actually an organization.".format(org_url))
        return None
    repos_info = github_request(project_info.get("repos_url"))

    if repos_info.status_code != 200:
        logger.warning("Couldn't get repository information for url: {}".format(project_info.get("repos_url")))
        return None

    g = GithubData(coin_id, org_url, [], [], [], [], [])
    repos_info_json = json.loads(repos_info.text)
    for info in repos_info_json:
        g += _get_all_historical_data(coin_id, info["url"], github_data)
    return g


def backfill_github_data(coin_id: int, repo_project_url: str) -> GithubData:
    g = get_for_coin_id(coin_id)
    logger.info("Processing github for coin: {}".format(repo_project_url))

    # Only get stats for the single repository in this case
    if not _is_github_org(repo_project_url):
        ret = _get_all_historical_data(coin_id, repo_project_url, g)
    else:
        ret = _accumulate_historical_data(coin_id, repo_project_url, g)

    # Insert the updated github data back into the database
    if ret is not None:
        insert_for_github_data(ret)
    else:
        logger.warning("No data found for {}".format(repo_project_url))

    return ret
